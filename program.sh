#!/bin/bash
case $1 in
	read)
		case $2 in
			eno)
				cat employee.txt | grep "$3,"
			;;
			name)
				cat employee.txt | grep ",$3,"
			;;
			salary)
				cat employee.txt | grep ",$3"
			;;
		esac		
	;;
	write)
	 	echo "$2,$3,$4" >> employee.txt
		echo "Done"
	;;
	update)
		a=$(cat employee.txt | grep "$2,")
		sed -i "s/$a/$2,$3,$4/g" employee.txt	
		echo "Done"
	;;
	delete)
		b=$(cat employee.txt | grep "$2,")
		sed -i "s/$b//g" employee.txt
		sed -i /^$/d employee.txt
		echo "Done"	
	;;
	duplicate)
		cat employee.txt | uniq -D | uniq 
	;;
	nthsalary)
#		c=$(cat employee.txt | tr "," " " | sort -k 3 -nr | uniq | head -n$2 | tail -n1 | cut -d" " -f3)
		c=$(cat employee.txt | tr "," " " | sort -k 3 -nr | cut -d" " -f3 | uniq | head -n$2 | tail -n1) 
		cat employee.txt | grep ",$c"
#		 cat employee.txt | tr "," " " | sort -k 3 -nr | awk -d"," '{if ($3 == "7") print $0;}'
	;;
esac

