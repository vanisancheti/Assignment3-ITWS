#!/bin/bash
read op
read n
for((i=1;i<=n;i++))
do
	read arr[$i]
done

m="\*"
d="/"
a=+
s=-
#for multiplication enter the input as \\*


if  [[ "$op" == '\*' ]]
then
	b=1
	for((i=1;i<=n;i++))
	do
		b=`echo "scale=4; $b*${arr[$i]}" | bc `
	done
	awk "BEGIN { printf \"%.4f\n\",$b }"	
elif [[ $op = $d ]]
then
        b=${arr[1]}
        for((i=2;i<=n;i++))
        do
                b=`echo "scale=4; $b/${arr[$i]}" | bc `
        done
        awk "BEGIN { printf \"%.4f\n\",$b }"       
elif [[ $op = $a ]]
then
        b=0
        for((i=1;i<=n;i++))
        do
                b=`echo "scale=4; $b+${arr[$i]}" | bc`
        done
        awk "BEGIN { printf \"%.4f\n\",$b }"       
elif [[ $op = $s ]]
then
        b=${arr[1]}
        for((i=2;i<=n;i++))
        do
                b=`echo "scale=4; $b-${arr[$i]}" | bc`
        done
        awk "BEGIN { printf \"%.4f\n\",$b }"      
fi


