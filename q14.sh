#!/bin/bash
echo "<!DOCTYPE html>"
echo "<head>"
echo "<title>LIST OF ALL DIRECTORIES AND FILES</title>"
echo "</head>"
echo "<body>"
echo "<table border="box" >"
echo "<th colspan="3"><center> <b>~</b> </center></th>"
echo "<tr><td><b>Name</b></td><td><b>Size</b></td><td><b>File/Dir</b></td></tr>"
	ls -l ~ | awk '{out=""; for(i=9;i<=NF;i++){out=out" "$i}; print out}' | sed '/^$/d' | while read a
        do
                size=$( du -sh ~/"$a" | awk '{ print $1 }')
#                if [ -d ~"${a}" ]
 #               then
  #                      type=Dir
   #             else
    #                    type=File
     #           fi         
		type=$( ls -l ~ | grep "$a" | cut -c1 )
		if [ "$type" == d ]
		then
			type="Dir"
		else
			type="File"
		fi
		echo "<tr><td>$a</td><td>$size</td><td>$type</td></tr>"
	done  
	ls -l ~ | egrep '^d' | awk '{out=""; for(i=9;i<=NF;i++){out=out" "$i}; print out}' | sed '/^$/d' | while read dir
	do
		if [ $( ls -l ~/"${dir}" | wc -l ) -ne 0 ] 
        	then
				echo "<th colspan="3"><center> $dir </center></th>"
				echo "<tr><td><b>Name</b></td><td><b>Size</b></td><td><b>File/Dir</b></td></tr>"
				ls -l ~/"${dir}" | awk '{out=""; for(i=9;i<=NF;i++){out=out" "$i}; print out}'| sed '/^$/d' | while read b
				do
					var=$(ls -l ~/"${dir}" | grep -w "$b" | cut -d" " -f1 | cut -c1)
					if [ "$var" == d ]
					then
						type=Dir
					else 
						type=File
					fi  
					size=$( du -sh ~/"${dir}"/"${b}" | awk '{ print $1 }' )
					echo "<tr><td>${b}</td><td>$size</td><td>$type</td></tr>"
			
				done
		fi
	done
echo "</table>"          
echo "</body>"
echo "</html>"

