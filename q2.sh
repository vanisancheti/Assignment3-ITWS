#!/bin/bash
HISTFILE=~/.bash_history
set -o history
#history 10 | tr -s " "  | tr " " "," | cut -d"," -f3 | sort | uniq -c | tr -s " " | awk '{ print $2 " "$1 }' | sort -k 2 -rn
#history 10 | cut -d" " -f1,2 --complement | cut -d"|" -f1 |  sort | uniq -c | tr -s " " | awk '{ print $2 " "$1 }' | sort -k 2 -rn
#history  | cut -d" " -f1,2,3 --complement 
history  | tr -s " " |  cut -d" " -f1 --complement | grep -v "|" | cut -d" " -f2 |  sort | uniq -c | tr -s " " | awk '{ print $2 " "$1 }' | sort -k 2 -rn
pipe=$(history  | tr -s " " |cut -d" " -f1 --complement | grep "|" | wc -l )
echo "PIPE $pipe"
