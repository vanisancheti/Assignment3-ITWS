#!/bin/bash
read a
n=$(echo $a|wc -m)
s=1
for((i=1;i<n;i++))
do
	arr[$i]=$(echo "$a" | tr '[:upper:]' '[:lower:]' | cut -c$i)
done
k=`expr $n/2`
j=`expr $n - 1`
for((i=1;i<=$k;i++))
do
	if [[ ${arr[$i]} != ${arr[$j]} ]]
	then
		s=0
		echo "no"
		break 
	else
		j=`expr $j-1`
	fi
done
if [ $s -eq 1 ]
then 
	echo "yes"
fi

